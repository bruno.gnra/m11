#! /bin/bash

# copia certificados
cp  /opt/docker/ldap.conf /etc/ldap/ldap.conf
mkdir /etc/ldap/certs
cp /opt/docker/ca.crt /etc/ldap/certs/.
cp /opt/docker/servercert.ldap.crt /etc/ldap/certs/.
cp /opt/docker/serverkey.ldap.pem  /etc/ldap/certs/.

# borrar config predeterminada
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

slaptest -f slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l  edt-org.ldif

# cambiar permisos per fer-los del user ldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# levantar el servicio 
/usr/sbin/slapd -d3 -h "ldap:/// ldaps:/// ldapi:///" && echo "slapd Ok"

