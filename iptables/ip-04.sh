#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #i/o # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# REGLAS ICMP
#-----------------------------------------

# No permeten fer pings a l'exterior
#iptables -A OUTPUT -p icmp --icmp-type 8 -j DROP

# No permeten fer ping cap al g25 
#iptables -A OUTPUT -p icmp --icmp-type 8 -d 10.200.245.225 -j DROP

# La nostra maquina no respon als pings que rep 
#iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP

# La nostra maquina no permet rebre pings 
#iptables -A INPUT -p icmp --icmp-type 8 -j DROP

# -----------------------------------------------------

# Reglas explicites permeto fer ping a la meva maquina # yo puedo trucar al que vulgui
iptables -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT  # request 
iptables -A INPUT -p icmp --icmp-type 0 -j ACCEPT   # respostas 

# Reglas explicites per denegar la nostra maquina respondre pings  # cuando em trucan no contesto 
iptables -A INPUT -p icmp --icmp-type 8 -j DROP # peticion input -> 8 
iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP # no contesto reply 


