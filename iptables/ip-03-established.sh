#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #INPUT/OUTPUT # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# Exemples de trafic related, established
# dos sentits de comunicacion 

# Permetre navegar por la web (port 80) 
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT

# Som un servidor web
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT

# Som un servidor web, pero el g25 esta vetat 
iptables -A INPUT -p tcp --dport 80 -s 10.200.245.225 -j DROP
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
