#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #i/o # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# fer nat per les xarxes docker netA(21) i netB(22) (dibuix pizarra) 
iptables -t nat -A POSTROUTING -s 172.21.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/16 -j MASQUERADE

# Regles forward 
# XarxaA no pot accedir a XarxaB # router no deja que busque cosas del 21 al 22 
#iptables -A FORWARD -s 172.21.0.0/16 -d 172.22.0.0/16 -j REJECT

# La xarxaA no pot accedir a B1
#iptables -A FORWARD -s 172.21.0.0/16 -d 172.22.0.2/16 -j REJECT

# La xarxaA no pot accedir a cap port 13 (de enlloc) 
#iptables -A FORWARD -p tcp -s 172.21.0.0/16 --dport 13 -j REJECT

# La xarxa A no pot accedir al port 2013 de la xarxaB
#iptables -A FORWARD -p tcp -s 172.21.0.0/16 -d 172.22.0.2/16 --dport 2013 -j REJECT


# La xarxa A pot navegar por internet pero res mes a l'exterior 
# web 80, sortida ethernet publica 
#iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 80 -j ACCEPT
#iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -p tcp --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
#(continuacion) todo el trafico de sortida de la xarxa a cap afora xapado # contraregla 
#iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -j REJECT
#iptables -A FORWARD -s 172.21.0.0/16 -i enp1s0 -j REJECT

# La xarxa A pot accedir al port 2013 de totes les xarxes d'internet (exterior) execpte d'aula 2hisx # exterior todo lo que va de la interficie publica cap fora # primero se hace la restrictiva 
iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 2013 -d 10.200.245.0/24 -j REJECT
iptables -A FORWARD -s 10.200.245.0/24 -p tcp --sport 2013 -i enp1s0 -d 172.21.0.0/16 -j REJECT

iptables -A FORWARD -s 172.21.0.0/16 -o enp1s0 -p tcp --dport 2013  -j ACCEPT
iptables -A FORWARD -d 172.21.0.0/16 -i enp1s0 -p tcp --sport 2013 -m state --state RELATED,ESTABLISHED -j ACCEPT 


