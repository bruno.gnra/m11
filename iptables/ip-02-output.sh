#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #INPUT/OUTPUT # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# Regles output 

# Podem accedir a qualsevol port 13 
iptables -A OUTPUT -p tcp --dport 13 -j ACCEPT

# No es port accedir a cap 2013 de ningu 
iptables -A OUTPUT -p tcp --dport 2013 -j REJECT

# No es port accedir a cap 3013 de ningu 
iptables -A OUTPUT -p tcp --dport 3013 -j DROP

# Port 4013 de ningu, aula si, g25 no 
iptables -A OUTPUT -p tcp --dport 4013 -d 10.200.245.225 -j DROP
iptables -A OUTPUT -p tcp --dport 4013 -d 10.200.245.0/24 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 4013 -j REJECT

# Port 5013 tothom si, aula no, g25 si 
iptables -A OUTPUT -p tcp --dport 5013 -d 10.200.245.225 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 5013 -d 10.200.245.0/24 -j DROP
iptables -A OUTPUT -p tcp --dport 5013 -j ACCEPT

# A l'aula nomes es pot accedir al servei ssh (tots els altres serveis xapats) 
#iptables -A OUTPUT -p tcp --dport 22 -d 10.200.245.0/24 -j ACCEPT
#iptables -A OUTPUT -d 10.200.245.0/24 -j REJECT
