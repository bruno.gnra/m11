#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #i/o # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# fer nat per les xarxes docker netA(21) i netB(22)
iptables -t nat -A POSTROUTING -s 172.21.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/16 -j MASQUERADE

# Redireccio de ports
# port 5001 porti al 2013 de A1
iptables -t nat -A PREROUTING -p tcp --dport 5001 -j DNAT --to 172.21.0.2:2013

# port 5002 porta al port 22 del A1  # dnat -> cambiar el desti
iptables -t nat -A PREROUTING -p tcp --dport 5002 -j DNAT --to 172.21.0.2:22

# port 6000 va parar al port 22 del propi router 
iptables -t nat -A PREROUTING -p tcp --dport 6000 -j DNAT --to :22

#iptables -A FORWARD -i enp1s0 -d 172.19.0.0/16 -j REJECT

# por 80 connecti amb gmail @donde ha de rebotar  
iptables -t nat -A PREROUTUNG -p tcp --dport 80 -j DNAT --to 217.70.18.56:80
