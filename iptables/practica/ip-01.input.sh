#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #i/o # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# Jugant amb el port 80 s'han redefinit ports alternatius on aplicar regles de filtrat d'entrada.
# Port obert a tothom
#iptables -A INPUT -p tcp --dport 80 -j ACCEPT

# Port tancat a tothom amb reject
#iptables -A INPUT -p tcp --dport 80 -j REJECT

# Port tancat a tothom amb drop
#iptables -A INPUT -p tcp --dport 80 -j DROP

#Port tancat a tothom excepte hostA
iptables -A INPUT -p tcp --dport 80 -j 171.20.0.2 ACCEPT 
iptables -A INPUT -p tcp --dport 80 -j REJECT
