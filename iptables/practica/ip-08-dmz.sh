#! /bin/bash
# edt ASIX M11 Curs 2022-2023
# iptables 

#echo 1 > /proc/sys/net/ipv4/ip_forward

# reglas flush 

iptables -F 
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto 
iptables -P INPUT ACCEPT 
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost # manipulamos varias cadenas 
# agrega reglas #i/o # todo lo que # acciones a ejecutar con el paquete ACCEPT/REJECT/DROP
iptables -A INPUT -i lo -j ACCEPT 
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip 
iptables -A INPUT -s 10.200.245.211 -j ACCEPT 
iptables -A OUTPUT -d 10.200.245.211 -j ACCEPT

# De la xarxaA només es pot accedir del router/fireall als serveis: ssh i daytime(13)
iptables -A INPUT -s 172.20.0.0/16 -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -s 172.20.0.0/16 -p tcp --dport 13 -j ACCEPT
iptables -A OUTPUT -s 172.20.0.0/16 -p tcp --dport 22 -j ACCEPT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -s 172.20.0.0/16 -p tcp --dport 13 -j ACCEPT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -s 172.20.0.0/16 -p tcp --dport -j REJECT

#(2)#Cap exterior forward 
# forward -s xa -o interficie -d 13/22/80 -j accept | 
#         -s xa -i if -s 13/22/80 -m state .. -j accept 
# forward  -s xa -o if -reject/drop 

#(3)#  peticion/respuesta  /
# forward -s xa -d ipwb -d 80 -ACCEPT
# forward -d xa -s web -sport 80 -state ..accept 
# forward -s xa -d xz drop 

